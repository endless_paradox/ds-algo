#include<bits/stdc++.h>

using namespace std;

struct Rectangle {
    int length;
    int breadth;
};

struct Test {
    int A[5];
    int n;
};

int area(struct Rectangle r);
int areaByReference(struct Rectangle &r2);
void changeLength(struct Rectangle *p, int l);
void fun(struct Test t1);
struct Rectangle *fun1();

int main()  {


    // ---------------------------------------------
    //  Call By Value Example

    struct Rectangle r1 = {10,5};
    cout<<area(r1)<<endl;

    // ---------------------------------------------
    // Call By Reference Example

    struct Rectangle r2 = {10,5};
    cout<<areaByReference(r2)<<endl<<r2.length<<endl;

    // ---------------------------------------------
    // Call By Address Example

    struct Rectangle r = {10,5};
    changeLength(&r, 20);
    cout<<r.length<<endl;

    // ---------------------------------------------
    // Structure Array
    /* We can pass structure as parameter to function as value so it will copy the array also to the new Structure */

    struct Test t = {{2,4,6,8,10},5};
    fun(t);
    cout<<t.A[0]<<endl;

    // ---------------------------------------------
    // Creating Structure in Heap

    struct Rectangle *ptr = fun1();
    cout<<"Length: "<<ptr->length<<endl<<"Breadth: "<<ptr -> breadth<<endl;

    return 0;
}

int area(struct Rectangle r1)   {
    return r1.length * r1.breadth;
}

int areaByReference(struct Rectangle &r2)   {
    r2.length++;
    return r2.length * r2.breadth;
}

void changeLength(struct Rectangle *p, int l) {

    p->length = l;

}

void fun(struct Test t1)  {

    t1.A[0] = 10;

}

struct Rectangle *fun1()    {
    struct Rectangle *p;
    // p = new Rectangle;
    p = (struct Rectangle *)malloc(sizeof(struct Rectangle));
    p -> length = 15;
    p -> breadth = 7;
    return p;
}