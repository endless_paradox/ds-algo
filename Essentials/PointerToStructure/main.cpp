#include<bits/stdc++.h>
using namespace std;


struct Rectangle    {
    int length;
    int breadth;
};

int main()  {
    
    // ---------------------------------------------------
    // Basic Pointer to Structure

    // Rectangle r = {10,5};

    // cout<<r.length<<endl<<r.breadth<<endl;

    // Rectangle *p = &r;

    // cout<<"-----------------------------------"<<endl;
    // cout<<p->length<<endl;
    // cout<<p->breadth<<endl;

    // ----------------------------------------------------
    // Pointer to Structure in Heap

    Rectangle *p;
    // p = ( struct Rectangle *)malloc(sizeof(struct Rectangle));
    p = new Rectangle();

    p -> length = 15;
    p -> breadth = 7;

    cout<<p->length<<endl;
    cout<<p->breadth<<endl;

    return 0;

}