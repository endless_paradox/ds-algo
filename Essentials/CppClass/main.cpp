#include<bits/stdc++.h>

using namespace std;

class Rectangle {
    private:
        int length;
        int breadth;
    public:
        Rectangle() {

        }

        Rectangle(int length, int breadth)  {
            this -> length = length;
            this -> breadth = breadth;
        }

        void initialize(int length, int breadth)   {
            this->length = length;
            this->breadth = breadth;
        }

        int area()  {
            return this -> length * this -> breadth;
        }

        void changeLength(int length)   {
            this -> length = length;
        }

        void printValue()    {
            cout<<"Length: "<<this -> length<<endl<<"Breadth: "<<this -> breadth<<endl;
        }

        ~Rectangle()    {
            cout<<"Destroyed"<<endl;
        }
};

int main()  {

    // using constructor for initialization
    Rectangle r(10,5);

    // using function for initialization
    // r.initialize(10,5);
    r.printValue();
    int area = r.area();
    cout<<"Area: "<<area<<endl;
    r.changeLength(20);
    r.printValue();

}