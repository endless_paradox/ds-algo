#include<bits/stdc++.h>

using namespace std;

void swapByValue(int x, int y);

void swapByAddress(int *x, int *y);

void swapByReference(int &x, int &y);

int main()  {

    // ------------------------------------------------
    // Call by Value
    cout<<"Call By Value:"<<endl;

    int a1 = 10;
    int b1 = 20;
    swapByValue(a1, b1);
    cout<<"First Number: "<<a1<<endl<<"Second Number: "<<b1<<endl<<endl;

    // ------------------------------------------------
    // Call By Address

    cout<<"Call By Address:"<<endl;
    int a2 = 30;
    int b2 = 40;
    swapByAddress(&a2, &b2);
    cout<<"First Number: "<<a2<<endl<<"Second Number: "<<b2<<endl<<endl;


    // ------------------------------------------------
    // Call By Reference

    cout<<"Call By Reference:"<<endl;
    int a3 = 50;
    int b3 = 60;
    swapByReference(a3, b3);
    cout<<"First Number: "<<a3<<endl<<"Second Number: "<<b3<<endl<<endl;
    return 0;

}

void swapByValue(int x, int y)  {
    int temp;
    temp = x;
    x = y;
    y = temp;
}

void swapByAddress(int *x, int *y)  {
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

void swapByReference(int &x, int &y)    {
    int temp;
    temp = x;
    x = y;
    y = temp;
}