#include<stdio.h>
#include<stdlib.h>

struct Rectangle    {
    int length;
    int breadth;
};

int main()  {

    // ****************************************
    // Basic Pointers
    // int a = 10;

    // int *p;
    // p = &a;

    // printf("%d\n", a);
    // printf("Using Pointer: %d\n", *p);

    // ****************************************
    // Arrays Pinter in stack

    // int A[5] = {2,4,6,8,10};
    // int *p;
    // p = A;

    // for(int i = 0; i<5;i++) {
    //     printf("%d\n", p[i]);
    // }

    // ****************************************
    // Arrays Pinter in Heap

    // int *p;
    // p = (int *)malloc(5 * sizeof(int));

    // p[0]=10;
    // p[1]=15;
    // p[2]=14;
    // p[3]=21;
    // p[4]=31;


    // for(int i = 0; i<5;i++) {
    //     printf("%d\n", p[i]);
    // }

    // free(p);

    // ****************************************
    // Size of Pointers

    int *p;
    double *p1;
    float *p2;
    char *p3;
    struct Rectangle *p5;

    printf("%lu\n", sizeof(p));
    printf("%lu\n", sizeof(p1));
    printf("%lu\n", sizeof(p2));
    printf("%lu\n", sizeof(p3));
    printf("%lu\n", sizeof(p5));

    return 0;
}