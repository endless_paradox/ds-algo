#include<bits/stdc++.h>

using namespace std;

template<class T>
class Arithmatic    {
    private:
        T a;
        T b;
    public:
    // -----------------------------------------
    // Internal Declaration
    // Arithmatic()    {

    // }

    // Arithmatic(T a, T b)    {
    //     this->a = a;
    //     this->b = b;
    // }

    // T add()   {
    //     return this->b + this->b;
    // }

    // T sub()   {
    //     return this->a - this->b;
    // }

    // void setA(T a) {
    //     this->a = a;
    // }

    // T getA()  {
    //     return this->a;
    // }

    // void setB(T b) {
    //     this->b = b;
    // }

    // T getB()  {
    //     return this->b;
    // }

    //--------------------------------------------
    // External Decleration
    Arithmatic();
    Arithmatic(T a, T b);
    T add();
    T sub();
    void setA(T a);
    T getA();
    void setB(T b);
    T getB();
};

template<class T>
Arithmatic<T>::Arithmatic()    {

}

template<class T>
Arithmatic<T>::Arithmatic(T a, T b)    {
    this->a = a;
    this->b = b;
}

template<class T>
T Arithmatic<T>::add()   {
    return this->a + this->b;
}

template<class T>
T Arithmatic<T>::sub()   {
    return this->a - this->b;
}

template<class T>
void Arithmatic<T>::setA(T a) {
    this->a = a;
}

template<class T>
T Arithmatic<T>::getA()  {
    return this->a;
}

template<class T>
void Arithmatic<T>::setB(T b) {
    this->b = b;
}

template<class T>
T Arithmatic<T>::getB()  {
    return this->b;
}

int main()  {

    Arithmatic<int> a(10,5);
    cout<<a.add()<<endl<<a.sub()<<endl;
    Arithmatic<float> a1(1.5,1.2);
    cout<<a1.add()<<endl<<a1.sub()<<endl;
    return 0;

}