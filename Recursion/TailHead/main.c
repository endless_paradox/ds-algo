#include <stdio.h>
#include <stdlib.h>

void fun(int n);

void fun1(int n);

int main()
{
    int x = 3;
//    fun(x);
    fun1(x);

    return 0;

}

void fun(int n)  {

    if(n>0) {

        printf("%d\n", n);
        fun(n-1);

    }

}

void fun1(int n)    {

    if(n>0) {

        fun1(n-1);
        printf("%d\n", n);

    }

}
